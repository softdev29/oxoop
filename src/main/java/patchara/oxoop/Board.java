package patchara.oxoop;

public class Board {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player currentPlayer;
    private Player o;
    private Player x;
    private int count;
    private int row;
    private int col;
    public boolean win = false;
    public boolean draw = false;

    public Board(Player o, Player x) {
        this.o = o;
        this.x = x;
        this.currentPlayer = o;
        this.count = 0;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean isWin() {
        return win;
    }

    public boolean isDraw() {
        return draw;
    }

    public boolean setRowCol(int row, int col) {
        if (isWin() || isDraw()) {
            return false;
        }
        this.row = row;
        this.col = col;
        if (row > 3 || col > 3 || row < 1 || col < 1) {
            return false;
        }
        if (table[row - 1][col - 1] != '-') {
            return false;
        }
        table[row - 1][col - 1] = currentPlayer.getSymbol();
        if (checkWin()) {
            updateStat();
            this.win = true;
            return true;
        }
        if (checkDraw()) {
            o.draw();
            x.draw();
            this.draw = true;
            return true;
        }
        count++;
        switchPlayer();
        return true;
    }

    public void updateStat() {
        if (this.currentPlayer == o) {
            o.win();
            x.loss();
        } else {
            o.loss();
            x.win();
        }
    }

    public boolean checkDraw() {
        if (count == 8) {
            return true;
        }
        return false;
    }

    public void switchPlayer() {
        if (currentPlayer == o) {
            currentPlayer = x;
        } else {
            currentPlayer = o;
        }
    }

    private boolean checkWin() {
        if (checkVertical()) {
            return true;
        } else if (checkHorizontal()) {
            return true;
        } else if (checkX()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean checkVertical() {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkHorizontal() {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkX() {
        if (checkX1()) {
            return true;
        } else if (checkX2()) {
            return true;
        }
        return false;
    }

    public boolean checkX1() { // 1,1 2,2 3,3
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkX2() { // 1,3 2,2 3,1
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }
}
